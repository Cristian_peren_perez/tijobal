<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Psr\Log\LoggerInterface;

class LuckyController extends AbstractController
{
    // /**
    //  * @Route("/lucky/number")
    //  */
    // public function number(): Response
    // {

    /**
     * @Route("/lucky/number/{max}")
     */
    public function number($max, LoggerInterface $logger)
    {
        $logger->info('We are logging!');

        $number = random_int(0, $max);

        return $this->render('lucky/number.html.twig', [
            'number' => $number
        ]);
        
    }
}
