<?php

namespace App\Entity;

use App\Repository\ColaboradorRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * Colaborador
 *
 * @ORM\Table(name="colaborador")
 * @ORM\Entity(repositoryClass=ColaboradorRepository::class)
 */
class Colaborador
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=50, unique=true)
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="primer_nombre", type="string", length=255)
     */
    private $primerNombre;

    /**
     * @var string
     *
     * @ORM\Column(name="segundo_nombre", type="string", length=255)
     */
    private $segundoNombre;

    /**
     * @var string
     *
     * @ORM\Column(name="primer_apellido", type="string", length=255)
     */
    private $primerApellido;

    /**
     * @var string
     *
     * @ORM\Column(name="segundo_apellido", type="string", length=255)
     */
    private $segundoApellido;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo_identificacion", type="string", length=255)
     */
    private $tipoIdentificacion;

    /**
     * @var \Date|null
     *
     * @ORM\Column(name="fecha_nacimiento", type="date", nullable=true)
     */
    private $fechaNacimiento;

    /**
     * @var bool
     *
     * @ORM\Column(name="activo", type="boolean")
     */
    private $activo;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var \Date|null
     *
     * @ORM\Column(name="create_time", type="datetime", nullable=true)
     */
    private $createTime;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get codigo.
     *
     * @return string
     */
    public function getCodigo() {
        return $this->codigo;
    }

    /**
     * Set codigo.
     *
     * @param string codigo
     *
     * @return Colaborador
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get primerNombre.
     *
     * @return string
     */
    public function getPrimerNombre() {
        return $this->primerNombre;
    }

    /**
     * Set primerNombre.
     *
     * @param string PrimerNombre
     *
     * @return Colaborador
     */
    public function setPrimerNombre($primerNombre)
    {
        $this->primerNombre = $primerNombre;

        return $this;
    }

    /**
     * Get segundoNombre.
     *
     * @return string
     */
    public function getSegundoNombre() {
        return $this->segundoNombre;
    }

    /**
     * Set segundoNombre.
     *
     * @param string $nombre
     *
     * @return Colaborador
     */
    public function setSegundoNombre($segundoNombre)
    {
        $this->segundoNombre = $segundoNombre;

        return $this;
    }
    
    /**
     * Get primerApellido.
     *
     * @return string
     */
    public function getPrimerApellido() {
        return $this->primerApellido;
    }

    /**
     * Set primerApellido.
     *
     * @param string $primerApellido
     *
     * @return Colaborador
     */
    public function setPrimerApellido($primerApellido)
    {
        $this->primerApellido = $primerApellido;

        return $this;
    }
    
    /**
     * Get segundoApellido.
     *
     * @return string
     */
    public function getSegundoApellido() {
        return $this->segundoApellido;
    }

    /**
     * Set segundoApellido.
     *
     * @param string $segundoApellido
     *
     * @return Colaborador
     */
    public function setSegundoApellido($segundoApellido)
    {
        $this->segundoApellido = $segundoApellido;

        return $this;
    }

    /**
     * Get fechaNacimiento.
     *
     * @return \DateTime|null
     */
    public function getFechaNacimiento() {
        return $this->fechaNacimiento;
    }

    /**
     * Set fechaNacimiento.
     *
    * @param \DateTime|null fechaNacimiento
     *
     * @return Colaborador
     */
    public function setFechaNacimiento($fechaNacimiento)
    {
        $this->fechaNacimiento = $fechaNacimiento;

        return $this;
    }
    
    /**
     * Get activo.
     *
     * @return string
     */
    public function getActivo() {
        return $this->activo;
    }

    /**
     * Set activo.
     *
     * @param string $activo
     *
     * @return Colaborador
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return Colaborador
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    public function getTipoIdentificacion(): ?string
    {
        return $this->tipoIdentificacion;
    }

    public function setTipoIdentificacion(string $tipoIdentificacion): self
    {
        $this->tipoIdentificacion = $tipoIdentificacion;

        return $this;
    }

    public function getCreateTime(): ?\DateTimeInterface
    {
        return $this->createTime;
    }

    public function setCreateTime(?\DateTimeInterface $createTime): self
    {
        $this->createTime = $createTime;

        return $this;
    }
}
