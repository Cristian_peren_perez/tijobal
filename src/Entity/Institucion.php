<?php

namespace App\Entity;

use App\Repository\InstitucionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * Institucion
 *
 * @ORM\Table(name="institucion")
 * @ORM\Entity(repositoryClass=InstitucionRepository::class)
 */
class Institucion
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=50, unique=true)
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="razon_social", type="string", length=255, nullable=false)
     */
    private $razonSocial;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_comercial", type="string", length=255, nullable=false)
     */
    private $nombreComercial;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo_identificacion", type="string", length=255)
     */
    private $tipoIdentificacion;

    /**
     * @var \Date|null
     *
     * @ORM\Column(name="fecha_fundacion", type="date", nullable=true)
     */
    private $fechaFundacion;

    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="string", length=255, nullable=true)
     */
    private $direccion;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="activo", type="boolean")
     */
    private $activo;

    /**
     * @var \Date|null
     *
     * @ORM\Column(name="create_time", type="datetime", nullable=true)
     */
    private $createTime;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get razonSocial.
     *
     * @return string
     */
    public function getRazonSocial() {
        return $this->razonSocial;
    }

    /**
     * Set razonSocial.
     *
     * @param string $razonSocial
     *
     * @return Institucion
     */
    public function setRazonSocial($razonSocial)
    {
        $this->razonSocial = $razonSocial;

        return $this;
    }

    /**
     * Get nombreComercial.
     *
     * @return string
     */
    public function getNombreComercial() {
        return $this->nombreComercial;
    }

    /**
     * Set nombreComercial.
     *
     * @param string $nombreComercial
     *
     * @return Institucion
     */
    public function setNombreComercial($nombreComercial)
    {
        $this->nombreComercial = $nombreComercial;

        return $this;
    }

    /**
     * Get fechaFundacion.
     *
     * @return string
     */
    public function getFechaFundacion() {
        return $this->fechaFundacion;
    }

    /**
     * Set fechaFundacion.
     *
     * @param string $fechaFundacion
     *
     * @return Institucion
     */
    public function setFechaFundacion($fechaFundacion)
    {
        $this->fechaFundacion = $fechaFundacion;

        return $this;
    }

    /**
     * Get direccion.
     *
     * @return string
     */
    public function getDireccion() {
        return $this->direccion;
    }

    /**
     * Set direccion.
     *
     * @param string $direccion
     *
     * @return Institucion
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get director.
     *
     * @return string
     */
    public function getDirector() {
        return $this->director;
    }

    /**
     * Set director.
     *
     * @param string $director
     *
     * @return Institucion
     */
    public function setDirector($director)
    {
        $this->director = $director;

        return $this;
    }

    /** 
     * Get activo.
     *
     * @return string
     */
    public function getActivo() {
        return $this->activo;
    }

    /**
     * Set activo.
     *
     * @param string $activo
     *
     * @return Institucion
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    public function getTipoIdentificacion(): ?string
    {
        return $this->tipoIdentificacion;
    }

    public function setTipoIdentificacion(string $tipoIdentificacion): self
    {
        $this->tipoIdentificacion = $tipoIdentificacion;

        return $this;
    }

    public function getCreateTime(): ?\DateTimeInterface
    {
        return $this->createTime;
    }

    public function setCreateTime(?\DateTimeInterface $createTime): self
    {
        $this->createTime = $createTime;

        return $this;
    }

    public function getCodigo(): ?string
    {
        return $this->codigo;
    }

    public function setCodigo(string $codigo): self
    {
        $this->codigo = $codigo;

        return $this;
    }
}
