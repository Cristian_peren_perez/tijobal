# git pull &&
composer install --prefer-dist --optimize-autoloader &&\
sudo chmod -R 777 var/cache var/log &&\
php bin/console cache:clear &&\
php bin/console cache:clear --env=prod &&\
php bin/console cache:warmup &&\
php bin/console cache:warmup --env=prod &&\
sudo chmod -R 777 var/cache var/log
