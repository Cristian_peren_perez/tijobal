git pull
if ($?) { ICACLS var\cache /grant Cristian:F /T }
if ($?) { ICACLS var\log /grant Cristian:F /T }
if ($?) { composer install --prefer-dist --optimize-autoloader }
if ($?) { php bin/console cache:clear }
if ($?) { php bin/console cache:warmup }
if ($?) { php bin/console cache:clear --env=prod }
if ($?) { php bin/console cache:warmup --env=prod }
if ($?) { ICACLS var\cache /grant IIS_IUSRS:F /T }
if ($?) { ICACLS var\cache /grant IUSR:F /T }
if ($?) { ICACLS var\log /grant IIS_IUSRS:F /T }
if ($?) { ICACLS var\log /grant IUSR:F /T }
if ($?) { php bin/console doctrine:schema:update --dump-sql }
